var Cardb = require('../model/cars');

// create and save new 
exports.create = (req, res) => {
    // validate request
    if (!req.body) {
        res.status(400).send({ message: "Data tidak boleh kosong!" });
        return;
    }

    // new data
    const car = new Cardb({
        name: req.body.name,
        rentPerday: req.body.rentPerday,
        size: req.body.size,
    })

    console.log(req.file.path);

    if (req.file) {
        const image = req.file.path;
        car.foto = image;
    }

    // save data in the database
    car
        .save(car)
        .then(data => {
            // res.send(data);
            req.flash('notify', 'Data Berhasil Disimpan!')
            res.redirect('/');
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating a create operation"
            });
        });

}

// Gett all data
exports.find = (req, res) => {

    if (req.query.id) {
        const id = req.query.id;

        Cardb.findById(id)
            .then(data => {
                if (!data) {
                    res.status(404).send({ message: "Id tidak ditemukan " + id })
                } else {
                    res.send(data)
                }
            })
            .catch(err => {
                res.status(500).send({ message: "Error retrieving car with id " + id })
            })
    }
    else {
        Cardb.find()
            .then(car => {
                res.send(car)
            })
            .catch(err => {
                res.status(500).send({ message: err.message || "Terjadi kesalahan saat mengambil informasi" })
            })
    }

}

// Get by id
exports.findId = (req, res) => {

    const id = req.params.id;

    Cardb.findById(id)
        .then(data => {
            if (!data) {
                res.status(404).send({ message: "Id tidak ditemukan " + id })
            } else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({ message: "Error retrieving car with id " + id })
        })

}

// Update data
exports.update = (req, res) => {

    if (!req.body) {
        return res
            .status(400)
            .send({ message: "Data yang akan diperbarui tidak boleh kosong" })
    }

    // cara 3
    const id = req.params.id;
    const body = req.body;

    const name = body.name;
    const rentPerday = body.rentPerday;
    const size = body.size;

    const updates = {
        name, rentPerday, size,
    };

    // console.log(req.file.filename)
    if (req.file) {
        // let foto = body.foto;
        // foto = "assets\\img\\car01.min.jpg"
        const image = req.file.path;
        updates.foto = image;
    }

    Cardb.findByIdAndUpdate(id, { $set: updates }, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `Cannot Update car with ${id}. Maybe user not found!` })
            } else {
                // res.send(data)
                // res.send("Data berhasil di update");
                req.flash('notify', 'Data Berhasil Diupdate!')
                res.redirect('/');
            }
        })
        .catch(err => {
            res.status(500).send({ message: "Error Update user information" })
        })
}

// Delete
exports.delete = (req, res) => {
    const id = req.params.id;

    Cardb.findByIdAndDelete(id)
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `Cannot Delete with id ${id}. Maybe id is wrong` })
            } else {
                res.send({
                    message: "Car was deleted successfully!"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete car with id=" + id
            });
        });
}