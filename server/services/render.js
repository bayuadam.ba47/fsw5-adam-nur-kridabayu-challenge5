
const axios = require('axios');

exports.index = (req, res) => {

    axios.get('http://localhost:3000/api/cars')
        .then(function (response) {
            res.render('index', { car: response.data });
        })
        .catch(err => {
            res.send(err);
        })

}

exports.add_cars = (req, res) => {
    res.render('add_cars');
}

exports.update_cars = (req, res) => {

    axios.get('http://localhost:3000/api/cars', { params: { id: req.query.id } })
        .then(function (response) {
            res.render('update_cars', { car: response.data });
        })
        .catch(err => {
            res.send(err);
        })

}