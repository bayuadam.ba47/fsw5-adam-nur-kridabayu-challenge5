const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require('body-parser');
const path = require('path');
const flash = require('express-flash-messages')
const session = require('express-session')

const connectDB = require('./server/database/connection');

const app = express();

// flash
app.use(flash());
app.use(session({
    secret: 'secret'
}));

// Moment
app.locals.moment = require('moment');

// Ambil port dari environment variable atau 8000
dotenv.config({ path: 'config.env' });
const PORT = process.env.PORT || 8000;

// Log request 
app.use(morgan('tiny'));

// mongodb connection
connectDB();

// parse request to body-parser
app.use(bodyparser.urlencoded({ extended: true }))

// Load assets
app.use('/assets', express.static('assets'));

// set view engine
app.set("view engine", "ejs");

// Load routers
app.use('/', require('./server/routes/router'));


app.listen(PORT, () => { console.log(`Server is running on http://localhost:${PORT}`) });