// $("#add_car").submit(function (event) {
//     alert("Data Inserted Successfully!");
// })


// $("#alert_update_car").submit(function (event) {
//     alert("Data  Update Successfully!");
// })


$("#update_car").submit(function (event) {
    event.preventDefault();

    var unindexed_array = $(this).serializeArray();
    var data = {}

    $.map(unindexed_array, function (n, i) {
        data[n['name']] = n['value']
    })

    var request = {
        "url": `http://localhost:3000/api/cars/${data.id}`,
        "method": "PUT",
        "data": data
    }


    $.ajax(request).done(function (response) {
        alert("Data Updated Successfully!");
    })

})


if (window.location.pathname == "/") {
    $ondelete = $(".delete");
    $ondelete.click(function () {
        var id = $(this).attr("data-id")

        var request = {
            "url": `http://localhost:3000/api/cars/${id}`,
            "method": "DELETE"
        }

        if (confirm("Apakah Anda benar-benar ingin menghapus data ini??")) {
            $.ajax(request).done(function (response) {
                // alert("Data Deleted Successfully!");
                let carsContainer = document.querySelector(".container4");
                let body = document.querySelector(".container5");

                let modal = `<div class="modal-dialog modal-confirm" >
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <div class="mid-item">
                                <img src="./assets/images/img-BeepBeep.png" class="material-icons">
                            </div>
                        </center>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                    </div>
                    <div class="modal-body">
                        <h4 class="modal-title">Menghapus Data Mobil</h4>
                        <p>Setelah dihapus, data mobil tidak dapat dikembalikan</p>
                    </div>
    
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Ya</button>
                            <button type="button" class="btn btn-danger">Tidak</button>
                        </center>
                    </div>
                </div>
            </div>`;

                let card = `<div class="alert alert-container alert-color-black alert-dismissible fade show"role="alert">
                                <strong>Data Berhasil Dihapus!</strong> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`;

                location.reload();
                body.innerHTML = modal;
                carsContainer.innerHTML = card;

            })
        }

    })
}




/*
if (window.location.pathname == "/") {
    $ondelete = $(".delete");
    $ondelete.click(function () {
        var id = $(this).attr("data-id")

        var request = {
            "url": `http://localhost:3000/api/cars/${id}`,
            "method": "DELETE"
        }

        if (confirm("Apakah Anda benar-benar ingin menghapus data ini??")) {
            $.ajax(request).done(function (response) {
                // alert("Data Deleted Successfully!");
                let carsContainer = document.querySelector(".container4");
                let card = `<div class="alert alert-container alert-color-black alert-dismissible fade show"role="alert">
                                <strong>Data Berhasil Dihapus!</strong> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`;

                location.reload();
                carsContainer.innerHTML = card;

            })
        }

    })
}
*/
